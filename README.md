**This repo will not be used anymore! Since all its content has been moved to the main ogs repo (MR 4442 and 4438), this one will not receive any updates, fixes or content expansion**


# Hitchhiker's Guide to the OpenGeoSys

This repository contains a working version of a guide to the OpenGeoSys intended for beginners. It is based on notes I made for my reference when I was learning how to use it, so it is neither complete nor verified yet. 

**Warning!** The content of this repo is a work in progress. There may be changes without notice and quality issues (broken links, typos, etc).

## Structure
Each folder is its own "Section". Folder Content contains files that it is not decided yet where they belong. 

[Click here to start browsing it](main.md)
