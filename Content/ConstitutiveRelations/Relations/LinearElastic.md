# Linear Elastic Constitutive Relation

There are two available version of this relation: [isotropic](LinearElastic.md#linear-elastic-isotropic) and [orthotropic](LinearElastic.md#linear-elastic-orthotropic).

## Linear Elastic Isotropic

It requires Young's Modulus ($E$) and Poisson's ratio ($\nu$) provided as [parameters](../../Project_file/Blocks/Parameters.md):

```xml
<constitutive_relation>
    <type>LinearElasticIsotropic</type>
    <youngs_modulus>E</youngs_modulus>
    <poissons_ratio>nu</poissons_ratio>
</constitutive_relation>
...
<parameters>
    <parameter>
        <name>E</name>
        <type>Constant</type>
        <value>5000000000</value>
    </parameter>
    <parameter>
        <name>nu</name>
        <type>Constant</type>
        <value>0.3</value>
    </parameter>
<parameters>
```

## Linear Elastic Orthotropic