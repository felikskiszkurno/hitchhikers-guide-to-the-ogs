# Processes - overview

Details on how to define process in the project file can be found [here](../Project_file/Blocks/Processes.md)

Following processes are available:
- [ThermoHydroMechanics](ThermoHydroMechanics.md)
- [ThermoRichardsMechanics](ThermoRichardsMechanics.md)
- [ComponentTransportProcess](https://www.opengeosys.org/docs/userguide/process-dependent-configuration/hydro_component/)

## Phases availability in processes

## Parameter supported by different processes
