# Creating project file

In this section of the tutorial, a [project file](/Content/Other/Glossary.md#project-file) will be created. Content will be added block by block. The discussed blocks will be presented directly in the text, but it may be convenient to open a [complete tutorial project file]() in separate tab or window to see each block in the context of the whole file. 

*TODO: Add line numbers to blocks, to help users find them in the full file*

## Conventions
In code block ```...``` indicate that one or more lines of code has been omitted to improve readability. The lines separated by ```...``` may not be in the same tag level, please consult the [complete tutorial project file]() to see specific code line in the context of the complete project file. 

## Starting point for creating a project file
Before starting to write a project file, it is important to plan the simulation, that it is supposed to describe.

There are many aspects, that have to be decided about and OpenGeoSys offers multiple option for many of them, therefore creating an optimal project file is not always a trivial task. It also means, that there is more than one correct way of setting up each simulation. It is up to the user to decide what will be best and most convenient for them.

This tutorial will guide you step by step through creating the project file from scratch in order to provide you with understanding of the structure and internal logic of it, but it is strongly recommended to use one of [benchamrks]() project files as a starting point and reshape it to match your specific requirements. Inspecting the available examples, can allow you to explore and discover different options that may be useful for your experiment.

## Description of the experiment
*TODO: Maybe move this part to jupyter notebook?*

### Parameters
Parameters were taken from Point heat source benchmark.
*TODO: Order entries in table in the same order as in prj file*
|Name|Defined in section:|Parameter section name|Value & Unit|
|---|---|---|---|
|Poissons ratio|Parameters|PoissonsRatioClay|0.25|
|Young's modulus|Parameters|YoungsModulusClay|4e9|
|Solid density|Media (0) - Solid phase properties|-|2290 kg/m^3|
|Solid thermal conductivity|Media (0) - Solid phase properties|-|1.838|
|Solid specific heat capacity|Media (0) - Solid phase properties|-|917.654|
|Solid thermal expansivity|Media (0) - Solid phase properties|-|1.5e-5|
|Porosity|Media (0) - properties|-|0|
|Biot coefficient|Media (0) - properties|-|1.0|
|Thermal conductivity|Media (0) - properties|-|1.838|
|Solid density|Media (1) - Solid phase properties|-|2290|
|Solid thermal conductivity|Media (1) - Solid phase properties|-|1.838|
|Solid specific heat capacity|Media (1) - Solid phase properties|-|917.654|
|Solid thermal expansivity|Media (1) - Solid phase properties|-|1.5e-5|
|Water specific heat capacity|Media (1) - Liquid phase properties|-|4280.0|
|Water thermal conductivity|Media (1) - Liquid phase properties|-|0.6|
|Water density|Media (1) - Liquid phase properties|-|Linear function|
|Water thermal expansivity|Media (1) - Liquid phase properties|-|4.0e-4|
|Water viscosity|Media (1) - Liquid phase properties|-|1.0e-3|
|Porosity|Media (1) - properties|-|0.16|
|Biot coefficient|Media (1) - properties|-|1.0|
|Thermal conductivity|Media (1) - properties|-|From OGS function|
|Permeability|Media (1) - properties|-|2e-20 0 0 2e-20|
|   |   |   |   |


### Initial conditions
Values of all initial conditions are defined in "Parameters" Section.
|Process variable|Parameter name|Value & Unit|  |
|---|---|---|---|
|Temperature|external_boundary_condition_temperature|20°C|   |
|Pressure|pressure_ic|   |   |
|Displacement|dirichlet0|0|   |
|   ||   |   |

### Boundary conditions
Values of all boundary conditions are defined in "Parameters" Section.
|Mesh|Process variable|Parameter name|Value & Unit|  
|---|---|---|---|
|simple_mesh_physical_group_Right_media|Temperature|temperature_init_cond|20°C|
|simple_mesh_physical_group_Left_media|Temperature|temperature_init_cond|20°C| 
|simple_mesh_boundary|Pressure|pressure_bc|   |
|simple_mesh_domain|Displacement|dirichlet0|0 0 0|
|   ||   |   |


## Initialization of the project file

The starting point is to provide xml version and encoding to ensure, that the file will be read correctly:

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
```
Next, the main tag is introduced to the file:

```xml
<OpenGeoSysProject>
    ...
</OpenGeoSysProject>
```

All the information required to create a simulation are placed inside this tag. 


## Meshes
The definition of meshes used for simulation is contained inside ```<meshes> </meshes>``` tag. 

It this section all files used to define mesh have to be listed. In the simplest experiments (only one medium with no source term) only the first two ones defining simulation domain and boundary have to be used. In this experiment, three additional ones are needed: one for the source term and two for the two media withing the simulation domain. 

```xml
<meshes>
    <mesh axially_symmetric="true">simple_mesh_domain.vtu</mesh>
    <mesh axially_symmetric="true">simple_mesh_boundary.vtu</mesh>
    <mesh axially_symmetric="true">simple_mesh_physical_group_Source.vtu</mesh>
    <mesh axially_symmetric="true">simple_mesh_physical_group_Right_media.vtu</mesh>
    <mesh axially_symmetric="true">simple_mesh_physical_group_Left_media.vtu</mesh>
</meshes>
```
More details on this section can be found [here](/Content/Project_file/Blocks/Meshes.md).

## Processes
In this section, the model of physical process for the simulation is defined. There can be multiple ones defined for one experiment, as long as they are placed in separate ```<process> </process>``` tags and have name that allows to distinguish them and is provided inside ```<name> </name>``` tag:
```xml
<name>simple_simulation</name>
```

In this experiment a thermo-hydro-mechanical (THM) process:
```xml
<type>THERMO_HYDRO_MECHANICS</type>
```
 with isotropic linear elastic [constitutive relation](/Content/Other/Glossary.md#constitutive-relation):
 ```xml
<constitutive_relation id="0">
    <type>LinearElasticIsotropic</type>
    <youngs_modulus>YoungsModulusClay</youngs_modulus>
    <poissons_ratio>PoissonsRatioClay</poissons_ratio>
</constitutive_relation>
 ```
is used. Variables "YoungsModulusClay" ($\Epsilon$) and "PoissonsRatiosClay" ($\nu$) are defined in [parameters section](./2_CreatingProjectFile.md#parameters-for-constitutive-relation). Those are required by [linear elastic](/Content/ConstitutiveRelations/Relations/LinearElastic.md) [constitutive relation](/Content/Other/Glossary.md#constitutive-relation). 

The dimensionality of the simulation has to be provided in this block too:

```xml
<dimension>2</dimension>
```

### Process variables
Next mandatory part is definition of process variables. They are dependent on the selected type of process. For THM they are: 
* temperature
* pressure 
* displacement

and they are defined as follows:
```xml
<process_variables>
    <temperature>temperature</temperature>
    <pressure>pressure</pressure>
    <displacement>displacement</displacement>
</process_variables>
```
Optionally, [secondary variables](/Content/Project_file/Blocks/Processes.md#secondary-variables) can be declared. In this experiment apart from temperature, pressure and displacement it is assumed that stress ($\sigma$) is of interest. Therefore, it will be declared here using it's internal name - "sigma" and output name - "stress", that user is allowed to choose:

```xml
<secondary_variables>
    <secondary_variable internal_name="sigma" output_name="stress"/>
</secondary_variables>
```
tag ```<secondary_variable>``` can be used multiple times as long as each of them refers to different internal name.

 More details on this section can be found [here](/Content/Project_file/Blocks/Processes.md).

 ## Media
Media represent areas with separate sets of physical parameters in the experiment. They can be representing different geological layers of elements made out of different materials. Multiple phases within the same layer/unit are represented as one medium. In this experiments two medias will be considered. The tag where those information stored is defined as follows:
```xml
<media> </media>
```

The medias are identified by the Material IDs. They are automatically assigned to each cell in the mesh by the msh2vtu script and always are consecutive, positive integers.

The figure below illustrated split between left medium with Material ID - 1 and right one with 0. 

 ![Mesh with Material IDs](./Figures/Material_IDs.png)

All phases and their properties for each medium have to be contained inside following tag:
```xml
<medium id="MediaID">
</medium>
```

In this experiment medium with Material ID 0 (from now on referred to as Layer0) will have only solid phase and the one with ID 1 (Layer1) will have both solid and liquid phases. The structure defining this setup can be written as follows:
```xml
<media>
    <medium id="0">
    <!--- Left side medium -->
        <phases>
            <phase>
                <type>Solid</type>
                <properties>
                    ...
                </properties>
            </phase>

            <properties>
                ...
            </properties>
        </phases>
    </medium>

    <medium id="1">
    <!-- Right side medium -->
        <phases>

            <phase>
                <type>AqueousLiquid</type>
                <properties>
                    ...
                </properties>
            </phase>
                    
            <phase>
                <type>Solid</type>
                <properties>
                    ...
                </properties>
            </phase>

            <properties>
            <!-- properties apply to the whole medium and don't belong to solid and liquid phases specifically -->
                ...
            </properties>
        
        </phases>
    </medium>
</media>
```
Tags ```<properties> </properties>``` are always populated with all parameters defining relevant physical properties of each medium and phase. As you can see from the example above inside the multiphase medium, there is a separate tag ```<properties> </properties>``` independent from the one in both phases. There should be stored all parameters that are representative for the whole medium, wheres parameters in phases are only valid for specific phase in medium. 

Apart from the parameter mentioned above, effective thermal conducitivity will be used. It is a feature specific to THM and TRM processes and it is enabled with following block:
```xml
<property>
    <name>thermal_conductivity</name>
    <type>EffectiveThermalConductivityPorosityMixing</type>
</property>
```
For more details click [here](/Content/Processes/ThermoHydroMechanics.md#thermal-porosity-mixing).

Keep in mind, that this is not available for all processes. Other ones may have features that will be specific to them as well. Those specific features are described in [process documentation]() or having a look at project files of [benchmarks]() utilizing the process of your interest. 

Rest of the parameter will be defined as constants using following template:

```xml
<property>
    <name>name_of_property</name>
    <type>Constant</type>
    <value>value_float_or_int</value>
</property>
```

It is simplest and safest (will work for all properties in all processes) way. Here is is used to reduce the complexity of the experiment presented, but in real practice, it may not be sufficient to defined all parameters as constants. In such a case [Parameters](/Content/Project_file/Blocks/Parameters.md) may be useful. However there are [limitations in reagard to what types are supported by processes](/Content/Project_file/Blocks/Media.md#supported-types-of-properties-and-special-cases).

Keep in mind, that "name_of_property" is not user defined. It has to contain one of the properties supported and/or require by specific process. 

 More details on this section can be found [here](/Content/Project_file/Blocks/Media.md).

 ## Time loop
There has to be one tag ```<process> </process>``` for each process defined in section [processes](2_CreatingProjectFile.md#processes), which is referenced by parameter "ref" in the ```<process> </process>``` tag. 

```xml
<time_loop>
    <processes>
        <process ref="simple_simulation">

        </process>
    </processes>
</time_loop>
```
Usually the first information inside of the ```<process> </process>``` tag is the name of nonlinear solver that is used. It has to match one of the defined in ```<nonlinear_solver> </nonlinear_solver>``` tag (discussed in [later part of this tutorial](#nonlinear-solvers)). 


Next, convergence criteria and corresponding thresholds have to be defined. This is a very sensitive step, as setting it too low may lead the simulation to fail (if the error tolerance is to low, OpenGeoSys, may not be able to find solution satisfying it) or to inaccurate results (if the tolerances are to high and search for solution will end prematurely). There are multiple [convergence criteria](/Content/Project_file/Blocks/Time_loop.md#convergence-criteria) available. For this experiment PerComponentDeltaX will be used. It requires a separate error tolerance for each component - here: variables declared in [process variables](2_CreatingProjectFile.md#process-variables-1). Keep in mind, that displacement is directional variables, so it requires separate tolerances in X, Y and Z directions.
```xml
<convergence_criterion>
    <type>PerComponentDeltaX</type>
    <norm_type>NORM2</norm_type>
    <!--reltols>T p u_x u_y u_z</reltols-->
    <reltols>1e-10 1e-10 1e-6 1e-6 1e-6</reltols>
</convergence_criterion>
```

Following block selects backward Euler method for time discretization (more details and alternative choices can be found [here](/Content/Project_file/Blocks/Time_loop.md#time-discretization))

```xml
<time_discretization>
    <type>BackwardEuler</type>
</time_discretization>
```

An appropriate choice of time stepping is important for the correctness and feasibility of the simulation. Too short may lead to incorrect results, or may not converge, too long will increase computational time (this is especially important for bigger and more complex experiments). Time stepping can be defined in [multiple ways](/Content/Project_file/Blocks/Time_loop.md#time-stepping). In this example fixed time stepping will be used. A predefined number of timesteps will be executed with the same duration for each of them. The information required by each type of time stepping differs. For fixed ones initial and end times of the simulation need to be provided and at least one pair of number of time steps and $\Delta t$ for them. It is possible to define multiple pairs, for example if smaller time steps are required at certain parts of the simulation. Here one pair is provided for simplicity:
```xml
<time_stepping>
    <type>FixedTimeStepping</type>
    <t_initial>0</t_initial>
    <!--<t_end>1075*24*60*60</t_end>-->
    <t_end>92880000</t_end>
    <timesteps>
        <pair><repeat>107</repeat>
                <delta_t>868037</delta_t></pair>
    </timesteps>
</time_stepping>
```
Keep in mind, that OpenGeoSys assumes consistent set of units is used in the simulation. If your experiment contains time dependent variables or parameters (for example flow) time parameters (initial and end time, $\Delta t$) have to be provided in a unit that will be consistent with them. In this tutorial no time dependent variables are used and time parameters are provided in seconds. 


 
 More details on this section can be found [here](/Content/Project_file/Blocks/Time_loop.md).

 ## Parameters
### Parameters for constitutive relation
Since the constitutive relation is isotropic, only one value for each parameter has to be provided.

For Young's modulus:

 ```xml
<parameter>
    <name>YoungsModulusClay</name>
    <type>Constant</type>
    <value>4e9</value>
</parameter>
```
and Poisson's ratio:
```xml
<parameter>
    <name>PoissonsRatioClay</name>
    <type>Constant</type>
    <value>0.25</value>
</parameter>
 ```

 More details on this section can be found [here](/Content/Project_file/Blocks/Parameters.md).

 ## Process variables
 Each variable defined in tag '<process_variables>' in Process section has to have an entry in this block. It is where initial conditions, boundary conditions and source terms will be defined. For the simplicity, this tutorial will define all values directly as constants, but there are more ways of doing it, more details can be found in [Process variables page](/Content/Project_file/Blocks/ProcessVariables.md). 

As all process variables are defined using the same pattern, only the blocks for temperature will be listed in this tutorial, for remaining blocks, see [complete tutorial project file](simple_simulation.prj).

 ### Initial conditions
The tag ```<initial_condition> </initial_condition>``` contains a name of parameter that represents the desired value that process variable should take at $t=0$. For example initial temperature could be defined in following way:
```xml
...
<initial_condition>temperature_init_cond</initial_condition>
...
<parameter>
    <name>temperature_init_cond</initial_condition>
    <type>Constant</type>
    <value>273.15+20</value>
</parameter>
```
Here the initial temperature is set to 20°C, but in order to be consistent with other parameters and variables, it is provided in Kelvins. OpenGeoSys doesn't have an internal validator, that would ensure global consistency of units, so it is up to user to define and use them consistently.

 ### Boundary and initial conditions
In order to set the heater to constant temperature of 50°C a simple [Dirichlet](/Content/Project_file/Blocks/Misc/BoundaryConditions.md#dirichlet) [boundary condition](/Content/Project_file/Blocks/ProcessVariables.md#boundary-conditions) can be used. 

Similarly to initial condition, this value will be provided as constant parameter:
```xml
<parameter>
    <name>external_boundary_condition_temperature</name>
    <type>Constant</type>
    <value>293.15</value>
</parameter>
```

As you can see it is the same value as used for the initial condition. This is not mandatory, boundary condition can take any value as long as it is valid.

For vectorial process variables both boundary and initial conditions should be provided for all components. It can be done as in the example above with one small change. The tag ```xml <value> </value>``` has to be replaced with ```xml <values> </values>``` as in following example for displacement:

```xml
<parameter>
    <name>displacement_ic</name>
    <type>Constant</type>
    <value>0 0</value>
</parameter>
```

This defines no displacement in any direction at the beginning of the simulation. The number of components has to match the number of dimensions defined in tag ```xml <dimension> </dimension>``` in process block. 


The boundary condition requires following information:
- name of the mesh file (as it was provided in "mesh" section) at which it should be applied
- type of the boundary condition
- component number ( has to match the number of dimensions defined in tag ```xml <dimension> </dimension>``` in process block)
- name of parameter containing the desired value

Considering this, it would be defined using following block:
```xml
<boundary_condition>
    <mesh>simple_mesh_physical_group_Right_media</mesh>
    <mesh>simple_mesh_physical_group_Left_media</mesh>
    <type>Dirichlet</type>
    <component>0</component>
    <parameter>external_boundary_condition_temperature</parameter>
</boundary_condition>
```
As you can see, in the ```<mesh>``` tag, the extension of the mesh file (".vtu") has been omitted. You can also notice, that this tag appears twice within ```<boundary_condition>``` tag. In fact ```<mesh>``` tag can be used as many times as necessary to apply the same boundary condition to different parts of the mesh. In this simulation, all external boundaries share the same boundary conditions, but the boundaries themselves has been split to two: surrounding left and right medium respectively. This was done to avoid confusion, but it is valid to use one file to define boundary surrounding multiple media. It works the other way around too! One medium can be surrounded by multiple boundary meshes. If they are supposed to have different values, the tag ```<boundary_condition>``` will have to be repeated for each mesh separately.  

### Source term
There is a separate tag ```<source_term>```, that can be used to defined a source term, but it is slightly more complicated, therefore in this tutorial, heat source will be defined using Dirichlet boundary condition. You can read about using tag ```<source_term>``` [here](/Content/Project_file/Blocks/ProcessVariables.md#source-term).

In order to define source term as it was described in the previous paragraph, the same tag structure as in previous section can be used. The only difference are the names of the mesh and the parameter:
```xml
<parameter>
    <name>heat_source_temperature</initial_condition>
    <type>Constant</type>
    <value>273.15+50</value>
</parameter>
...
<boundary_condition>
    <mesh>simple_mesh_physical_group_Source</mesh>
    <type>Dirichlet</type>
    <component>0</component>
    <parameter>heat_source_temperature</parameter>
</boundary_condition>
```
Here, the mesh containing source has been set to have a constant temperature of 50°C throughout the whole simulation time.

*Warning:* If the tag ```<source_term>``` will not appear at least once in the project file, OpenGeoSys will issue a warning in the output log, but the simulation still can and will be run if there are no other problems. 

 ## Linear solvers
 OpenGeoSys offers many different types of linear solvers, that may differ in the way they need to be defined in. More details on available solvers and their definition can be found at page: [Linear solvers](/Content/Project_file/Blocks/Linear_solvers.md).

 Describing the details of solver used for this simulation is beyond scope of this tutorial, so it will only be provided as a full block:

 ```xml
<linear_solvers>
    <linear_solver>
        <name>general_linear_solver</name>
        <lis>-i bicgstab -p ilu -tol 1e-16 -maxiter 10000</lis>
        <eigen>
            <solver_type>BiCGSTAB</solver_type>
            <precon_type>ILUT</precon_type>
            <max_iteration_step>10000</max_iteration_step>
            <error_tolerance>1e-8</error_tolerance>
            <scaling>1</scaling>
        </eigen>
        <petsc>
            <prefix>sd</prefix>
            <parameters>-sd_ksp_type cg -sd_pc_type bjacobi -sd_ksp_rtol 1e-16 -sd_ksp_max_it 10000</parameters>
        </petsc>
    </linear_solver>
</linear_solvers>
 ```

 ## Nonlinear solvers
Similarly to linear solvers, OpenGeoSys allows users to select one of many options. It is not in the scope of this tutorial to dive into details of a specific solver therefore whole block will be provided:

```xml
<nonlinear_solvers>
    <nonlinear_solver>
        <name>basic_newton</name>
        <type>Newton</type>
        <max_iter>50</max_iter>
        <linear_solver>general_linear_solver</linear_solver>
    </nonlinear_solver>
</nonlinear_solvers>
```

**Warning:** It may be counter intuitive, but in the project files tag ```<nonlinear_solvers>``` often is placed before ```<linear_solvers>``` even though first one has to refer to the second one. It will not cause problem, as OpenGeoSys reads the whole content of project file and than interprets it, so the exact order of tags and blocks is not strictly defined.
*TODO: verify with Dima*

 More details on this section can be found [here](/Content/Project_file/Blocks/Nonlinear_solvers.md).

 ## Summary
 The complete project file, containing everything discussed in the part of the tutorial can be found [here](./simple_simulation.prj).
 
## Next step
Now with both mesh and project file ready, simulation can be run. Click [here](3_Running_experiment.md)