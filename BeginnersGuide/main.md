# Beginners Guide

This tutorial will guide you step by step by the whole workflow of running an experiment with OpenGeoSys. From creating mesh, preparing project file, to executing it and analyzing results.

## Description of the problem
Rectangular domain with circular source in the middle, split in half into two media with different thermal conductivity. Both media are two phase porous layers.

## Glossary
In [Glossary]() you can find definitions of some terms, that new users may not be familiar with or are used in a non standard way. 

## Step 1: Create mesh
Click [here](1_CreatingMeshGmsh.md) to create simple mesh.

## Step 2: Prepare project file
In this section you will be guided through creating an example project file from scratch. 

## Step 3: Execute simulation
This section will introduce different ways of running OpenGeoSys, their advantages and limitations.

## Step 4: Analyze results
Some exemplary tools that can be used to work with results delivered by OpenGeoSys will be introduced here. 

## Step 5: Working with project file
This section will show how to work with project files by using a python package. It will allow to run multiple experiments with varying parameters automatically.

## Problems
If you encounter any problems following this guide, you can consult [Frequently Asked Questions]() to see if you can find help there. 

## Benchmark
This experiment is also available in more condensed form as jupyter notebook in the [benchmark gallery]().

