# Running experiment
Each section of this part of the tutorial will assume, that the used software is already set up on your machine. If it is not the case yet, please follow [installation guide]() specific to the way you would like to run OpenGeoSys in.

## Running OpenGeoSys with command line
The simplest way of running a simulation with OpenGeoSys is to use command line. This assumes, that OpenGeoSys is [installed natively](/Content/Other/Glossary.md#native-installation). To do so [download binaries of ogs]() or [compile it from source]().

```bash
ogs simple_simulation.prj
```
This will place all the resulting files in the same folder as the "simple_simulation.prj" file.


## Running OpenGeoSys with Python
Follow [install via pip guide](). Ensure that the active python environment is the same as the one OpenGeoSys was installed in and follow the previous section.

## Running OpenGeoSys in a container
See [Running OGS in a container guide]().

# Summary
All methods mentioned above deliver the same results and are equally supported/valid. You are free to choose the one that fits you use case, hardware and system setup best. For more advanced simulations, you may need to consider limitations specific to each method of installing OpenGeoSys. 

# Next step
After obtaining results from OpenGeoSys, next step is to [analyse them](4_Evaluation_of_results.md).