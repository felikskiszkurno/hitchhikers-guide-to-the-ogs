# Frequently asked questions

## Is OpenGeoSys the greatest?
Yes, it is.

## Oh, no! It diverges!
OpenGeoSys will sometimes crash with an error message stating, that it couldn't reduce the time step any more. There are several things that can be done to solve this:
- In [Time loop](../Project_file/Blocks/Time_loop.md) adjust the [error tolerance](../Project_file/Blocks/Time_loop.md#error-tolerances)
- Check if [mesh quality](../Project_file/Blocks/Meshes.md#mesh-quality) is sufficient
- Verify if all [parameters](../Project_file/Blocks/Parameters.md), especially those that vary during the simulation, make physical sense through out the whole experiment and combined with other parameters.

## Oh, no! There are NaNs everywhere!
If in the output log all values are [NaN](Glossary.md#nan) and one or more parameters were defined as functions, there is a good chance that there is an imaginary number somewhere, probably due to OpenGeoSys taking a root of negative value.

## What is the Answer to the Ultimate Question of Life, the Universe, and Everything?
Easy! Of course it is 42! :)

