// Constants
center_x = 0;
center_y = 0;
center_z = 0;
length = 100; // length of each side of the square domain
radius = 1; // radius of the source

// Central point
Point( 1) = {center_x, center_y, center_z};

// External boundary
Point( 2) = {center_x+length/2, center_y+length/2, center_z};
Point( 3) = {center_x+length/2, center_y-length/2, center_z};
Point( 4) = {center_x-length/2, center_y-length/2, center_z};
Point( 5) = {center_x-length/2, center_y+length/2, center_z};

// Spherical heat source
Point( 6) = {center_x, center_y+radius, center_z};
Point( 7) = {center_x+radius, center_y, center_z};
Point( 8) = {center_x, center_y-radius, center_z};
Point( 9) = {center_x-radius, center_y, center_z};

// Media separator
Point(10) = {center_x, center_y+length/2, center_z};
Point(11) = {center_x, center_y-length/2, center_z};

// Lines external boundary
Line( 1) = { 2,  3};
Line( 2) = { 3, 11};
Line( 3) = {11,  4};
Line( 4) = { 4,  5};
Line( 5) = { 5, 10};
Line( 6) = {10,  2};

// Lines separating media
Line( 7) = {10,  6};
Line( 8) = {11,  8};

// Source
Circle ( 9) = {6, 1, 7};
Circle (10) = {7, 1, 8};
Circle (11) = {8, 1, 9};
Circle (12) = {9, 1, 6};

// Curve loop for right media
Curve Loop( 1) = {6, 1, 2, 8, -10, -9, -7};

// Curve loop for left media
Curve Loop( 2) = {3, 4, 5, 7, -12, -11, -8};

// Create Surfaces
Plane Surface( 1) = 1;
Plane Surface( 2) = 2;

// Create physical groups
Physical Curve("Source") = {9, 10, 11, 12};

Physical Surface("Right_media") = {1};
Physical Surface("Left_media") = {2};

// MESHING
Mesh.ElementOrder = 1;
Mesh 2;
