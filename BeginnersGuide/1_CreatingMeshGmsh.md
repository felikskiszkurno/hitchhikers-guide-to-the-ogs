# Creating mesh in Gmsh
There is no one strict and specific way of creating mesh for simulation run with OpenGeoSys. As long as at the end there is a set of files readable for OpenGeoSys, all methods are equally valid. In this example Gmsh will be used, but there are other options for example: .

Gmsh can be used as GUI application, with its own scripting language or via Python, Julia or C++ APIs. Here Gmsh scripting language will be used. 

## Example mesh
The example mesh used here is defined as square 100 meters by 100 meters with round source in the middle of it with radius of 1 meter. The full script file used to create this mesh can be found [here](simple_mesh.geo). Note that in order to improve the readability of this guide, there are slight differences in which commands are used compared to the script file.

## Creating points defining external boundary, split between media and source
In this section points defining external boundary and source will be created.

Let's assume that the center of the mesh is at (0, 0) and define variables describing what is fixed in the mesh:

```cpp
// Constants
center_x = 0;
center_y = 0;
center_z = 0;
length = 10; // length of each side of the square domain
radius = 1; // radius of the source
```

First step and most simple entity, that can be created with Gmsh is points. Syntax to create it is following:
```cpp
Point(id)={x_coordinate, y_coordinate, z_coordinate};
```
Setting the right id is very important. It is not done automatically. No two entities of the same dimensionality can share the same value of id. For example to one dimensional group belong: lines, curves and polylines. Hence, the ids cannot repeat for those entities. The id has to be a positive integer greater than 0. 

Now, we can define center point of the mesh, which will coincide with center of the circular source:
```cpp
// Center of mesh and source term
Point( 1) = {center_x, center_y, center_z};
```
4 points defining external boundary:
```cpp
Point( 2) = {center_x+length/2, center_y+length/2, center_z};
Point( 3) = {center_x+length/2, center_y-length/2, center_z};
Point( 4) = {center_x-length/2, center_y-length/2, center_z};
Point( 5) = {center_x-length/2, center_y+length/2, center_z};
```
4 points that will be used to create source:
```cpp
Point( 6) = {center_x, center_y+radius, center_z};
Point( 7) = {center_x+radius, center_y, center_z};
Point( 8) = {center_x, center_y-radius, center_z};
Point( 9) = {center_x-radius, center_y+radius, center_z};
```

Additionally on top and bottom horizontal boundaries, two points are needed that will be used to create an interface (internal boundary) between two medias:
```cpp
Point(10) = {center_x, center_y+length/2, center_z};
Point(11) = {center_x, center_y-length/2, center_z};
```

Now, we can create lines between points on the external boundary. The syntax for it is simple:
```cpp
Line(id) = {id_of_starting_point, id_of_end_point}
```

```cpp
Line( 1) = { 2,  3};
Line( 2) = { 3, 11};
Line( 3) = {11,  4};
Line( 4) = { 4,  5};
Line( 5) = { 5, 10};
Line( 6) = {10,  2};
```
and lines that will be later used to create the two medias:
```cpp
Line( 7) = {10,  6};
Line( 8) = {11,  8};
```

2->3->11->4->5->10->2 (make a diagram here)


Next step would be to use points 6 to 9 to create circle, which will be later used as heat source. Gmsh doesn't allow to create circles directly, instead we will create 4 circle arc and combine them into physical curve. The input parameters for Circle command are: first point, center, last point:
```cpp
Circle ( 9) = {6, 1, 7};
Circle (10) = {7, 1, 8};
Circle (11) = {8, 1, 9};
Circle (12) = {9, 1, 6};
```
Note, that the circles are identified using the same indicator as lines, therefore id for first circle has to have an increment from the last line defined before it -  it this case last line id is 8, so first circle has id 9. 

The lines on their own cannot be used to create a boundary. Now all lines that has been created are separate entities, whereas boundaries defining our simulation domain or media have to be single entities. How can the lines be transformed into an entity that can be a valid boundary? They need to be combined together, but joining them into a "curve loop". Contrary to what name suggests, "curve loop" doesn't need to contain curves only, it can contain lines, curves, polyline and any 1D Gmsh entity. 

Next, the curve loop defining medium on the right side of the simulation domain will be created:
```cpp
Curve Loop( 1) = {6, 1, 2, 8, -10, -9, -7};
```
Note, that some lines are listed with "-" sign. Lines in Gmsh have direction. For example, line one is defined between points 2 and 3 and its direction goes from point to 2 to 3. In order to create valid loop, this direction has to be respected. Image a person or a device following lines from point to point. It is possible to follow line 2 from point 3 to 11 and than line 8 from 11 to 8, but next step is defined with circle 10 from point 7 to 8. This is a problem because at the end of line 8, we are at point 8 and first point defined in line 10 is 7. The end of last step and the beginning of current one do not match. Imagine walking on tiles, you cannot take a step in which you will start at tile you are not on. Therefore line 10 has to be "reversed" and in Gmsh this is done by prefixing the line id with "-".
*Add diagram to make it easier to understand*
```cpp
Curve Loop( 2) = {3, 4, 5, 7, -12, -11, -8};
```

## Creating surface
Next step is to create surfaces. We will create one for each medium. Surface is defined inside closed curve loops. As with other entities, we create it by providing an id for a new object and id the curve loop encircling area at which surface is supposed to be created. For mesh for right medium:
```cpp
Plane Surface( 1) = {1};
```
and for the mesh for left medium:
```cpp
Plane Surface( 2) = {2};
```
ids on the right hand side refer to curve loops created for each of the medias.

Creating a surface is not sufficient on its own. It still needs to be filled with mesh. In this tutorial it is done as follows:
```cpp
Mesh.ElementOrder = 1;
Mesh 2;
```

## Physical groups
At this point, the geometrical part of mesh is created. But how can Gmsh distinguish between parts of the mesh that are supposed to be used for different purposes? To human, it is clear which part is boundary, which is source and which is mesh. But computers need those things to be defined explicably and specifically.

The first step in defining it, is to create what Gmsh calls "physical groups". Creating physical groups is important as later mesh created by Gmsh will be split into files readable for OpenGeoSys depending on their content. They can contain multiple entities. The special thing about them is, that they can be named using string. Gmsh by default will only export elements, that part of at least one physical group.

At this point, all elements required to define external boundary are available.Therefore a "physical curve" (type of "physical group") containing all lines, that are part of the source will be created: 
```cpp
Physical Curve("Source") = {9, 10, 11, 12};
```
Similarly, surfaces can and have to embedded in "physical groups":
```cpp
Physical Surface("Right_media") = {1};
Physical Surface("Left_media") = {2};
```
Differently than in the case of source and external boundary, those groups will contain not indices of curves, but those of "plane surfaces".


## Summary of mesh creation
At this point the script defining mesh is ready. The complete file can be found [here](simple_mesh.geo).


## Conversion to format readable by OpenGeoSys
Now, we can expert Gmsh mesh file:
```bash
gmsh -2 simple_mesh.geo -o simple_mesh.msh
```
in this command -2 indicates that mesh is supposed to be 2D and "simple_mesh.geo" is name of the script describing mesh we have just created. This can be done from the GUI as well. String after -o defines name and file format of the output (this is the mesh file). The result will be file "simple_mesh.geo" containing our mesh.

However, msh files are not readable for OpenGeoSys, they needed to be converted to vtu-files. Fortunately, there is a python script that does it: script [msh2vtu]() written by Dr. Dominik Kern from TUB Freiberg. 

```bash
python3 msh2vtu.py simple_mesh.msh -r -g
```
More details about using msh2vtu can be found [here](./UsefulTools.md#msh2vtu).

After running the command above, in 5 files should appear:
* simple_mesh_boundary.vtu
* simple_mesh_domain.vtu
* simple_mesh_physical_group_Source.vt
* simple_mesh_physical_group_Right_media.vtu
* simple_mesh_physical_group_Left_media.vtu

There are three files, that were created based on physical groups that were defined. First two files were extracted automatically by msh2vtu script: boundary and domain. Therefore it was not required to create explicit physical groups for those two entities.

Those files can be referred to in the project file and used to define media, boundary conditions and source. 

# Next step
Now, with the mesh ready, we can proceed to the next step: [creating project file for the experiment](2_CreatingProjectFile.md).