# Using MFront with OpenGeoSys

## Goal
[MFront](https://tfel.sourceforge.net/) can be used to run simulation with constitutive model not available in OpenGeoSys or with a modification of one of available ones. 

## Prerequisites
OpenGeoSys has to be compiled with flag ```-DOGS_USE_MFRONT```. It will enable MFront support and download necessary libraries. For more details about compiling OpenGeoSys, see [developer guide]().

## Preparing MFront file
The details of preparing MFront file will not be discussed here. Only the necessary details will be stated.

### Name of the model
At the beginning of the file, name of the model has to be provided as follows:
```c++
@Behaviour ModelName
```
The MFront file has to be the same as name provided in the line above:
```
ModelName.mfront
```
### Material properties
The material properties, that should be read from project file and be used in the MFront model have to be defined using following syntax: 
```c++
@MaterialProperty type MatPropName
```
The project file has to contain references pointing from one of the parameters to material properties used by the MFront model. For details, see next section.

## Preparing project file
In the project file, MFront model has to be selected as constitutive model in ```<process> </process>``` tag. See following example:
```xml
<constitutive_relation id="0">
    <type>MFront</type>
    <behaviour>ModelName</behaviour>
    <material_properties>
        <material_property name="MatPropName" parameter="ParameterName"/>
    </material_properties>
</constitutive_relation>
```
In this example ModelName has to match one of the MFront files in the OpenGeoSys source code. [Attribute]() "name" defines name of one of the material properties in the MFront file (see [next section](MFront.md#preparing-opengeosys-to-use-new-mfront-model) for details on how to add new model). 
The value of "parameter" [attribute]() in ```<material_property/>``` has to match one of the content of tag ```<name> </name>``` in one of the [parameters]() block. For example:
```xml
<parameter>
    <name>ParameterName</name>
    <type>Constant</type>
    <value>value_of_parameter_Name</type>
</parameter>
```
Tag ```<material_properties> </material_properties>``` can be used as many times as it is necessary. 

## Preparing OpenGeoSys to use new MFront model
After the MFront file has been prepared, it has to be placed in the folder containing OpenGeoSys source code at following path:
```
ogs-source-code/MaterialLib/SolidModels/MFront
```
in the same folder there is ```CMakeLists.txt```. In that file, there is a list containing names of all MFront models stored in the folder. Name of the newly added model has to be added to that list:
```c++
mfront_behaviours_check_library(
    ...
    ModelName
    ...
)
```
Now to, enable the new "ModelName", the "configure" and "generate" steps and OpenGeoSys has to be recompiled (this process should take less time than the first time, as only new code will be compiled, for more details see [developer guide]()). 

<div class='note'>
### Warning!
If a compilation error is encountered, it can be caused by inconsistent use of name of the model. It is case sensitive and it has to be written in exactly the same way everywhere, where it is used. 
</div>

## References
### Benchmarks using MFront


### Available MFront models

## Testing MFront model with MTest
Ensure following folders are listed in your ```LD_LIBRARY_PATH```:

```bash
.\
ogs-build-dir\lib
ogs-build-dir\lib64
```