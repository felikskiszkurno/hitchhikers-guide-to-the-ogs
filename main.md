# Hitchhiker's guide to OpenGeoSys

## Idea behind this guide

The idea of this repository is to contain a collection of notes of various aspects of using OpenGeoSys and explain them in a way that would lower the entry threshold for new users and provide them with some guidance written in non-technical, simple language. 

## How and where to start?
This guide is going to help you with using of OpenGeoSys. If you need help setting it up, compiling it or running it in a container, please see the [introduction page](https://www.opengeosys.org/docs/userguide/basics/introduction/).

Assuming you have an idea of what you want to simulate with OpenGeoSys, a good starting point would be the [Gallery of benchmarks](https://www.opengeosys.org/docs/benchmarks/). There you can find an overview of what processes are available in OpeGeoSys and examples of how to simulate them with all necessary files to execute them provided. Once you find a benchmark, that to some extent resemblances your experiment, you can have a look at [project file overview](/Project_file/main.md) to understand how it is structured and how it can be adjusted to match your needs. 

If you are still not sure which process is of interest to you, you can start be reading the [overview of processes available in OpenGeoSys](/Processes/main.md). 

If you still would like to create a project file from scratch (what is not recommended for first time users), please consult [minimum requirements for project file](/Project_file/Project_file_requirement.md) for details on what a valid project file needs to contain.

After you get comfortable with basic functionality of OpenGeoSys, please feel invited to explore [Advanced Features](/Advanced/main.md) that OGS offers. 

## Useful resources

[Project file overview](/Project_file/main.md)

The overview of all the processes that can be simulated using OpenGeoSys can be found [here](/Processes/main.md)

[List of useful software and tools](Content/Other/UsefulTools.md)

If you run into problems, have a look at [Frequently Asked Questions](/Content/Other/FAQ.md). It may guide you towards solution of your problem, or search [OpenGeoSys Discourse](https://discourse.opengeosys.org). It is possible, that somebody had similar problem before. 