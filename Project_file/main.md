# Project file
[Project file](../Other/Glossary.md#project-file) (with "prj" extension) is the way of describing to OpenGeoSys what experiment you intend to simulate. 

The syntax of the project file follows the syntax of a typical xml file and can be edited with any editor capable of working with this kind of files. 

If you are not familiar with structure of XML-files, this page will help you: [*.prj Project Files](Prj_file_intro.md)

The structure of the project file is relatively flexible: as long as the semantics of the file are intact, the main blocks can be provided in any order. 

Depending on what the simulation is about, the content of project file will differ. However, there are some basic minimum requirement that have to be fulfilled. 

Following blocks with valid content have to be provided in each project file, their content will be discussed in following pages: 
 - [Meshes](Blocks/Meshes.md)
 - [Process](Blocks/Process.md)
 - [Media](Blocks/Media.md)
 - [Time loop](Blocks/Time_loop.md)
 - [Process variables](Blocks/ProcessVariables.md)
 - [Nonlinear solvers](Blocks/Nonlinear_solvers.md)
 - [Linear solvers](Blocks/Linear_solvers.md)

 Blocks in this list are optional but may be convenient to use and are used in many benchmarks:
 - [Curves](Blocks/Curves.md)
 - [Parameters](Blocks/Parameters.md)
 - [Test definitions](Blocks/Test_definitions.md)
 - [Geometry](Blocks/Geometry.md)

 If you already have a project file but you are wondering how to modify it automatically or how it can be interacted with from external tools, please have a look at [Working with project files](WorkingWithProjectFile.md) section.