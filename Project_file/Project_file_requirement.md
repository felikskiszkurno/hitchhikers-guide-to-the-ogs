# Minimum requirements for a valid project file

Each OGS-Project file consists out of multiple section defining various aspects of the project. 

Generally there is a certain degree of flexibility, when it comes to the internal structure of the project file. Still, there are some exceptions. In "Time loop" section, "set stepping" subsection has to be defined after "add process". 

CHECK FOR MORE RULES LIKE THIS!!!

Following Sections discuss how to define various parts of the project file, each section corresponds with a part of project file with the same tag name. The order in which they are discussed is based on benchmark files, but their order in the project file can be modified.