# Nonlinear solvers
Following non-linear solvers are available in OpenGeoSys:
- [Newton](Nonlinear_solvers.md#newton)
- [Picard](Nonlinear_solvers.md#picard)

## Newton
The nonlinear solver of "Newton" type is the implementation of the Newton-Raphson method.
Basic definition of non-linear solver follows this template:

```xml
<nonlinear_solver>
    <name>basic_newton</name>
    <type>Newton</type>
    <max_iter>100</max_iter>
    <linear_solver>linear_solver</linear_solver>
</nonlinear_solver>
```



## Picard