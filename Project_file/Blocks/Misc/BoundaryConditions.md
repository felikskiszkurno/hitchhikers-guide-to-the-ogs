# Boundary conditions
All types of boundary conditions discussed in this article can be defined directly in the project file (more details can be found [here](/Content/Project_file/Blocks/ProcessVariables.md#boundary-conditions)). Additionally Dirichlet and Neumann boundary conditions can be defined using [Python boundary conditions functionality](/Content/Advanced/PythonBC.md).

## Dirichlet

### Constrained Dirichlet

### Dirichlet within time interval

### Primary variable constraint Dirichlet boundary condition

### Solution dependent Dirichlet

## Neumann
Can be used to define [source terms](./../ProcessVariables.md#sources) at the boundary of the mesh. 
### Non uniform variable dependent Neumann
### HC non advective free component flow boundary
### Normal traction

## Robin



## Python
Can be used to define [Dirichlet](#dirichlet) and [Neumann](#neumann) boundary conditions. More details can be found in [this article](/Content/Advanced/PythonBC.md).




