# Processes
Process block is defined with ```<process> </process>``` tags.

This section defines the process, that is modelled in the project.
It has to contain 'name' and 'type' tags:
```xml
<name>THM</name>
<type>THERMO_HYDRO_MECHANICS</type>
```
Name can be defined freely, type has to contain one of following strings [list of available processes](https://doxygen.opengeosys.org/d5/d98/ogs_file_param__prj__processes__process.html).
Click on the links in the list below, to find out more about specific processes:
- [ThermoHydroMechanics](../../Processes/ThermoHydroMechanics.md)
- [ThermoRichardsMechanics](../../Processes/ThermoRichardsMechanics.md)
- [ComponentTransportProcess](https://www.opengeosys.org/docs/userguide/process-dependent-configuration/hydro_component/)

## Integration order
Tag ```<integration_order> </integration_order>``` is mandatory and defines order of Legendre polynomials. *Check it*

## Process variables
An important part of this section is defined in "process\_variables" tag. It is important, because in later parts of the project files (e.g.: in definition of errors), the order in which those variables have been defined plays an important role. For more details see [time loop](Time_loop.md) and [linear solvers](./Linear_solvers.md). Those variables are specific to each type of process. Information of which variables are required for which process can be found in appropriate sections of doxygen documentation. For example, for THM process following parameters are required:
```xml
<process_variables>
	<temperature>temperature</temperature>
	<pressure>pressure</pressure>
	<displacement>displacement</displacement>
</process_variables>
```

## Secondary variables
This tag is optional. It allows to define which secondary variables will be computed and saved in the output files. Which secondary variables are available is dependent on a specific process.
They can be defined as follows:
```xml
<secondary_variables>
	<secondary_variable internal_name="internal_name" 			output_name="output_name"/>
</secondary_variables>
```
where internal name has to match the name of one of available secondary variables and output name can be defined by the user. Output name will be used as the name of the field into which specific variable will be written into in output files. 

## Error tolerances
In this section, within ```<convergence\_criterion>``` tag, relative tolerances for error have to be defined - with tag ```<reltols>```. Each value in this tag defines the tolerance for errors with respect to one process variables, in the same order as they were defined in <process\_variables> tag. For the process variables listed above, relative error tolerances can be defined as follows:
```xml
<reltols>
	reltol_temp
	reltol_press
	reltol_disp_0
	reltol_disp_1
</reltols>
```
The order can differ based on the order in which process variables has been defined and on dimensionality of the process.
Keep in mind, that some process variables have more than one values, as displacement in the example above. In such a case, a matching number of reltols has to be defined. 

## Constitutive relations
[Constitutive relation](../../Other/Glossary.md) can be one of the [existing relations](../../ConstitutiveRelations/ConstitutiveRelationsOverview.md) implemented in OpenGeoSys or it can be defined by user using [MFront](../../Advanced/MFront.md).

To define constitutive relation, tags ```<constitutive_relation> </constitutive_relation>``` are used. The fixed, minimum requirement is presence of ```<type> </type>``` tag. Other tags depend on the chosen relation. 

## Source terms
There are fourd available types of source terms:
- line
- nodal
- volumetric
- python

 The source term has to be scaled by the volume.  

## Specific body force
 The tag ```<specific_body_force>``` can be used to define gravitational force. By default it is not considered in the simulation, it has to be be explicitly enabled by setting an appropriate value in project file.
```xml
<process>
	...
    <specific_body_force>in_X_direction in_Y_direction in_Z_direction
    </specific_body_force>
    ...
</process>
```

In 3D case it has to be defined on Z-axis as negative value: $-9.81$. So the code in project file would look like this:
```xml
<process>
	...
    <specific_body_force>0 0 -9.81</specific_body_force>
    ...
</process>
```

**Warning!** If specific body force is added, it has to be reflected with the initial state of stress. It shouldn't be used with axially symmetrical meshes.

Stress is a a bit unique, as initial condition for stress are not defined in block "Process variables", but in "Process". It requires setting up a constant or function definition in "Parameters" block where initial stress is defined. It has to match the dimension of experiment. OGS expects it to be effective stress (total stress with pore pressure subtracted), this is in contrast to total stress that is expected to be provided in boundary condition. If meshes are defined as axially symmetrical, the stress has to be provided in spherical or cylindrical coordinates. 

## Jacobian Assembler