# Linear solvers

An important parameter defined for each linear solver is "error tolerance". Combined with "abstols" from section [Time loop](Time_loop.md) it defines the acceptable level of error in the obtained result. Those two parameters are interconnected with each other. Setting either of them too high will result in error message referring to problems with setting up smaller time step. As the variables are interconnected, it is possible to avoid error when one of them is set too low, by setting the other one a bit too high.

Generally, the error tolerance of linear solver should always be smaller than "abstols". Unlike "abstol", "error tolerance" is not specific to "process variables" and is defined as one value. For most cases value below $10^{-10}$ is recommended. 

## Eigen