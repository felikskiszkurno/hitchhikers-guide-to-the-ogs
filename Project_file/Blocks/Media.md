# Media
Inside of this block, all media in the simulation are defined. There has to be a medium for every value of Media ID used in he mesh. Those IDs are assigned when mesh is created or converted by msh2vtu script. 
```xml
<media>
	<medium id="MaterialID_matchin_mesh_file">

	</medium>
</media>
```

## Phases
Medium can consist of multiple phases. For examaple porous geological would consist of solid phase (for example clay) and liquid phase (e.g.: water in the pores). 


In media with multiple phases, each of them can occur maximal once per medium. Phases within one medium are distinguished by their type:
```xml
<medium id="0">
	<phases>
		<phase>
			<type>Solid</type>
		</phase>
	</phases>
</medium>
```

If only one phase is considered inside a medium, the definition of that phase can be reduced to placing ```<phases/>``` as follows:

```xml
<medium id="MaterialID">
	<phases/>
<medium>
```

The available phases depend on the type of the process. Overview can be found in

ADD OVERVIEW


## Properties

The properties share very similar structure with [parameters](), but they are not interchangeable. 

Generally, it is the most safe to use "Constant" type for properties and when it is not sufficient, type "Parameter" can be used as fallback. Still, there are some limitations to what types of parameter can be used in different processes. 

In "Properties", type "Curve" can be used to define parameter dynamically based on variable listed in [MPL->VariableType.h](https://gitlab.opengeosys.org/ogs/ogs/-/blob/master/MaterialLib/MPL/VariableType.h):

- capillary_pressure
- concentration
- density
- displacement
- effective_pore_pressure
- enthalpy_of_evaporation
- equivalent_plastic_strain
- grain_compressibility
- liquid_phase_pressure
- liquid_saturation
- mechanical_strain
- molar_mass
- molar_mass_derivative
- molar_fraction
- phase_pressure
- porosity
- solid_grain_pressure
- stress
- temperature
- total_strain
- total_stress
- transport_porosity
- vapour_pressure
- volumetric_strain
- number_of_variables

Keep in mind that not all of those variables will be available in all the processes. For example, in THM there is phase_pressure, but not liquid_phase_pressure. 

### Supported types of properties and special cases
