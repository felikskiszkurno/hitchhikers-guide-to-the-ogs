# Curves
This block can be used to define one or more 

They can be accessed from within ```<type>Curve</type>``` or ```<type>CurveScaled</type>``` parameters (more details on [curves](Parameters.md#curve) and [curves scales](Parameters.md#curvescaled)).

## Example
Curves can be defined using the following block:

```xml
<curves>
    <curve>
        <name>CurveName</name>
        <coords>
            x_1 x_2 ... x_n
        </coords>
        <values>
            y(x_1) y(x_2) ... y(x_n)
        </values>
    </curve>
</curves>
```
and can be called in "Properties" and "Parameters" inside "Expression":
```CurveName(evaluation_value)```

where ```evaluation_value``` is always exactly one value and refers to values provided inside ```<coords> </coords>```. So if the curve is dependent on time ```evaluation_value``` can be providing using variable $t$. The same applies for spatial curves, however they can only depend on only one of the spatial variables.
Curves can be useful to include information on variation of time dependent parameters. Those values can be called from 'equation' tag in 'parameter' if 'type' 'formula' is selected.

## Interpolation
*Check how are the values interpolated for coords between the given ones.*