# Parameters
This block is used to contain various parameters, that can be called from other blocks withing the project file. 

Use of this block is not strictly mandatory, but it can be quite convenient to keep all the parameters together. The possible content of this block in not limited to physical properties of materials used in the experiment but also to values of [boundary](ProcessVariables.md#boundary-conditions) and [initial conditions](./ProcessVariables.md#initial-conditions) or [source terms](./ProcessVariables.md#sources), physical constants, etc.


## Where can the parameters be used?
The parameters defined in this block can be used in blocks:
- [Media](Media.md)
- [Process variables](ProcessVariables.md)
- ...

## Parameters vs properties


## How to define a parameter?

To create a parameter withing the ```<parameters> </parameters>``` tag, following template can be used:

```xml
<parameter>
    <name>...</name>
    <type>...</type>
</parameter>
```

Tags ```<name> </name>``` and ```<type> </type>``` are mandatory and define human-readable name of specific parameter and declare one of available types with which it will be defined. Other tags depend on what is the content of ```<type> </type>```. There are following types available:
- [Constant](Parameters.md#constant)
- [Linear](Parameters.md#linear)
- [Function](Parameters.md#function)
- [Curve](Parameters.md#curve)
- [CurveScaled](Parameters.md#curvescaled)

Each of them will be discussed below. The same types can be used to define media properties in the [media block](Media.md).

### Constant
This is the most basic type. It only requires ```<value> </value>``` tag additionally where the value of the parameter is provided as a number. It will not change throughout the experiment. For example:
```xml
<parameter>
    <name>earth_acceleration</name>
    <type>Constant</type>
    <value>9.81<value>
</parameter>
```

### Linear
Type linear, can be used to declare parameters that vary linearly depending on value of another parameter. 

Apart from standard required tags: ```<name> </name>``` and ```<type> </type>```, ```<reference_value> </reference_value>``` has to be provided.

For one parameter, linear dependency on multiple (unlimited) independent variable can be defined as long as all of them are process variables (?).
Linear dependency on each independent variable requires ```<reference_condition> </reference_condition>``` and ```<slope> </slope>```.

Consider simple linear equation:
$$
y(x)=ax+b
$$
let's say that there is a parameter $y$ that depends linearly on temperature:
$$
y(T)=a*T+y_0
$$
Defining it as linear type would look like this in project file:
```xml
<property>
    <name>y</name>
    <type>Linear</type>
    <reference_value>y_0</reference_value>
    <independent_variable>
        <variable_name>temperature</variable_name>
        <reference_condition>T_0</reference_condition>
        <slope>a</slope>
    </independent_variable>
</property>
```
where reference value $y_0$ is defined as follows:
$$
y_0=y(T_0)
$$

More real example can be found in benchmark [A2](https://gitlab.opengeosys.org/ogs/ogs/-/blob/master/Tests/Data/HydroMechanics/A2/A2.prj), where fluid density linearly, but independently depends on pressure and temperature:

```xml
<property>
    <name>density</name>
    <type>Linear</type>
    <reference_value>1200</reference_value>
    <independent_variable>
        <variable_name>temperature</variable_name>
        <reference_condition>298.15</reference_condition>
        <slope>-6.0e-4</slope>
    </independent_variable>
    <independent_variable>
        <variable_name>phase_pressure</variable_name>
        <reference_condition>4e6</reference_condition>
        <slope>0.5e-9</slope>
    </independent_variable>
</property>
```
The definition of density provided in the snippet above can be express with following equations:
$$
\rho(T=298.15, p=4e6)=1200 
$$
$$
\rho(T)=(-6*10^{-4})*T+1200
$$
$$
\rho(p)=(0.5*10^{9})*p+1200
$$

### Function
Type function extends the basic structure of [constant type](Parameters.md#constant). There are two major difference though. The expression that defines function has to be place inside of ```<expression> </expression>``` tag nested inside of ```<value> </value>``` tag. Additionally, derivations with respect to applicable process variables have to be provided inside following this template:

```xml
<dvalue>
    <variable_name>primary variable derived over</variable_name>    
    <expression>expression of derivation of function w.r.t. specific primary variable </expression>
</dvalue>
```

#### Convention & Syntax
Powers are indicated by "^", e.g.: 2^3=8.

In scientific notation, power can be appended by leading zeros. So ```0.002=2e-3=2e-03```, those three expression will evaluate to the same value in OpenGeoSys.

C++ functions (for example std::pow) cannot be called from the expression tag. 

#### Example
Consider function defining viscosity of water depending on temperature:
$$
\mu=(-0.0002*T^3+0.05*T^2-4*T+178)*10^{-5}
$$
and its partial derivative with respect to temperature:
$$
\frac{\partial \mu}{\partial T}=(-0.0006*T^2+0.1*T-4)*10^{-5}
$$

With the two equations above, the full implementation of water viscosity depending on phase pressure and temperature will look as follows:
```xml
<property>
    <name>viscosity</name>
    <type>Function</type>
    <value>
        <expression>(-0.0002*(temperature-273.15)^3+0.05*(temperature-273.15)^2-4*(temperature-273.15)+178)*1e-5</expression>
    </value>
    <dvalue>
        <variable_name>temperature</variable_name>
        <expression>(-0.0006*(temperature-273.15)^2+0.1*(temperature-273.15)-4)*1e-5</expression>
    </dvalue>
</property>
```
There are limitations of what variables can be used inside of the ```<expression> </expression>``` tags. Only the ones related to the process variables can be called. The values defined for example in [parameter block](Parameters.md) are out of reach. CHECK THIS!!!

For example if ```thermal_conductivity``` is defined and density is provided as Function, the dvalue of density with respect to temperature will be ignored.

Notice, that OpenGeoSys assumes self consistent set of units. In the code snippet above temperature has to be converted from Kelvins (used by other temperature dependent parameters in the project from which this snippet comes) to degrees Celsius required by the provided formula. OpenGeoSys doesn't have an internal unit management system. It is users task to ensure, that the units used are consistent and compatible!


### Curve
Curve is a bit special type, as it requires presence of another block in the project file. Whether the parameter is defined in Parameters block, or anywhere else in the project file, type curve will always refer to curve defined in [curve block](Curves.md). Regardless, of whether there is only one curve defined or multiple, they are always identified by the content of tag ```<name> </name>```. 

![Relation between parameter of type curve and curves block](./Figures/curve.svg)

Parameter of type curve can be defined using the following template:

```xml
<property>
    <name>property_name</name>
    <type>Curve</type>
    <independent_variable>curve_name_coords</independent_variable>
    <curve>curve_name</curve>
</property>
```
where independent variable refers the values in tag ```<coords> </coords>``` in block ```<curves> </curves>```. For more details see [example of curve definition](Curves.md#example).

Type "Curve" is different to the one "Properties" tag. In the "Expression" tag only numerical values and x, y, z, t variables and curves can be used.

### CurveScaled
It requires tags: ```<curve> </curve>``` and ```<parameter> </parameter>```. The first one contains name of a curve defined in the ```<curves> </curves>``` block (for more detail see [curves](Curves.md)).


```xml
<parameter>
    <name>p_Dirichlet_right</name>
    <type>CurveScaled</type>
    <curve>p_Dirichlet_right_temporal</curve>
    <parameter>p_Dirichlet_right_spatial</parameter>
</parameter>
```

```xml
<curve>
    <name>p_Dirichlet_right_temporal</name>
    <coords>0.0 10.0</coords>
    <values>0.0 10.0</values>
</curve>
```

```xml
<parameter>
    <name>p_Dirichlet_right_spatial</name>
    <type>Constant</type>
    <value>1</value>
</parameter>
```

There is a hierarchy among parameters.


