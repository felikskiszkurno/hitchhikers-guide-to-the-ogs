# Process variables
All "process parameters" defined in "Process" Section has to be addressed here (see [process](Processes.md).

```xml
<process_variable>
    <name>"process_variable"</name>
    <components>integer</components>
    <order>integer</order>
    <initial_condition>variable_name</initial_condition>
    <boundary_conditions>
        <boundary_condition>
            <type>Dirichlet</type>
            <mesh>Temp_Dis_mesh_boundary</mesh>
            <component>1</component>
            <parameter>dirichlet0</parameter>
        </boundary_condition>
    </boundary_conditions>
</process_variable>
```
Tag "components" refers to how many directional components variable has, for example for displacement in 2D THM process will have 2 (for x and y directions)."initial conditions" should contain the name of variable defined in [parameters block](Parameters.md). 

## Boundary conditions
 There are two types of boundary conditions: [Dirichlet](./Misc/BoundaryConditions.md#dirichlet) and [Neumann](./Misc/BoundaryConditions.md#neumann). First one can be used to set boundary condition at a set value, Neumann can be used for flow (energy for example). They both can be used to add sources as well. Neumann has to be scaled. For more details on both issues, see [sources](ProcessVariables.md#sources).

## Initial conditions
Initial conditions define the state of the primary variables in the domain at $t=0$. Each process will require a different set of them, see [process variables](Process.md#process-variables) for more details. 

There are a three ways of providing a value for initial condition. The simplest one is to write it directly inside  of the ```<initial_condition> </initial_condition>``` tag. For example setting initial temperature to $273.15K$:
```xml
<name>temperature</name>
<initial_condition>273.15</initial_condition>
```
Alternatively, the value of initial temperature can be defined in [parameters block](Parameters.md) and called from ```<process_variable> </process_variable>``` by the content of ```<name> </name>``` tag. See following example:
```xml
...
    <name>temperature</name>
    <initial_condition>initial_temperature</initial_condition>
...
...
<parameter>
    <name>initial_temperature</name>
    <type>Constant</type>
    <value>273.15</value>
</parameter>

```

## Sources
Providing sources is not mandatory and will not prevent simulation from running. There are two ways how sources can be defined in the project file: as boundary condition and as source term.

### Source term

There are following types available: nodal, volumetric. 

```xml
<source_term>
    <geometrical_set>geometry</geometrical_set>
    <geometry>inner</geometry>
    <type>Nodal</type>
    <parameter>pressure_source_term</parameter>
</source_term>
```

### Source as boundary condition
Defining it as boundary condition can be done in the same way as it was described in the paragraph above. 

### Scaling a source 
Neumann boundary condition should be scaled by the surface of the source:

$$source\_scaled = source\_term/scaling\_factor [source\_strength\_unit/L^{dim-1}]$$
 
with unit as specified above where dim is the dimension. For example if the source is 2D, the $scaling\_factor$ will be the length of it's circumference (or the relevant part of it if the source is at the boundary of the domain) and similarly in 3D case, the relevant area. If the mesh is axially symmetric, it has to be considered that it is fully symmetric. The 2D source will be rotated by $360°$. For example a rectangular source at the axis of symmetry, will have to be considered as a cylinder. For scaling the surface of the cylinder has to be used,not the relevant part of the circumference of the rectangle. 

If source is placed on the axially symmetrical boundary and has radius equal to 0, it has to be defined as boundary condition not as source term. If radius is non zero, it can be defined using source term. 
