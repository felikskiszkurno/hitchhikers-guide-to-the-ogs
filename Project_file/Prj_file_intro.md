# *.prj - OpenGeoSys Project Files

The main input taken by OpenGeoSys is path to project file. 

*Note: this page is not a complete description of XML standard. It only provides enough information to interact with OpenGeoSys \*.prj files. For more details can be found here: [XML Standard](https://www.w3.org/standards/xml/).*

The XML files structure follows an abstract idea of a tree: from roots to leaves. The main tag within any XML file is called root. In OpenGeoSys project files the root tag looks like this: ```<OpenGeoSysProject> </OpenGeoSysProject>```. Between opening and closing tags all the project relevant information is contained. Everything between those tags follows a hierarchical structure. There will be thematic blocks that cover specific aspects of the simulation. They define for example mesh files, or process used in the simulation. Following snippet shows a small structure containing those two (for now empty) blocks:

```xml
<OpenGeoSysProject>
    <mesh>
    ...
    </mesh>
    
    <process>
    ...
    </process>
</OpenGeoSysProject>
```

The blocks ```<mesh> </mesh>``` and ```<processes> </processes>``` are at the same "level" indicated by the indent. Those are the child elements of the root ```<OpenGeoSysProject>```, which is their parent element. As they are on the same level, they can be described as siblings. This hierarchy is recursive. This means, that each of the child elements can have it's own child elements and be parent to them (think of family structure: grandparents -> parent -> children -> grandchildren).

In following snippet you can see a ```<processes> </processes>``` element containing child element ```<processes> </processes>```, which again contains two child elements: ```<name> </name>``` and ```<type> </type>```:

```xml
<OpenGeoSysProject>
    <process>
        <process>
            <name>SteadyStateDiffusion</name>
            <type>STEADY_STATE_DIFFUSION</type>
        </process>
    </process>
</OpenGeoSysProject>
```

This snippet can be understood in more human readable way as follows:

*In this project file used by OpeGeoSys there is one instance of processes, which is called SteadyStateDiffusion and is implemented in OpenGeoSys as type STEADY_STATE_DIFFUSION*.

## Attributes
If a value is set between ```<``` and ```>```, beside the tag, it is called attribute. For example:
```xml
<dog name="Woofer">
```
Entity "dog" has attribute "name" set to "Woofer".

In project files, attributes are used in some blocks to distinguished between multiple instances of the same entity. For example [media](./Blocks/Media.md#media) are identified by their attribute "id":
```xml
<medium id="0">
```
and processes in [time loop](./Blocks/Time_loop.md#process) by the "ref" attribute:
```xml
<process ref="SteadyStateDiffusion">
```

## Comments
If you want to switch between two version of a certain part of the project file, two values etc, but without need to remove those fragments completely from the file, you can comment them out:

```xml
<!--value>this value will be comment out and ignored by OpenGeoSys</value-->
<value>this value will be read and used by OpenGeoSys</value>
```

## More information

More details on what blocks are required/available in the project file and what can they contain, please see: [Project file over view](Project_file_overview.md).

## File encoding
In order to ensure correct encoding, it is a good practice to add folowing line at the beginning of the project file:

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
```

Are any other  encodings allowed?